<?php

namespace App\Repository;

use App\Entity\Key;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Key|null find($id, $lockMode = null, $lockVersion = null)
 * @method Key|null findOneBy(array $criteria, array $orderBy = null)
 * @method Key[]    findAll()
 * @method Key[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class KeyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Key::class);
    }

    /**
     * @return key []
     * @param $statusWorker string
     * @description
     * return key [] in function of parameter
     * if "worker" return key where worker-id IS NOT NULL
     * if "noWorker" return key where worker-id IS NULL
     *
     */
    public function workerNullOrNot ($statusWorker): array
    {
        $query = $this->createQueryBuilder('k');
        if($statusWorker == "worker")
            $query->where('k.worker IS NOT NULL')
                ->orderBy('k.worker', 'ASC');
        if($statusWorker == "noWorker")
            $query->where('k.worker IS NULL');
        return $query
            ->andWhere('k.status = 0')
            ->addOrderBy('k.reference', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }
    // /**
    //  * @return Key[] Returns an array of Key objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('k.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Key
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
