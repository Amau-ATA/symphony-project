<?php
// src/Controller/UserController.php
namespace App\Controller;


use App\Entity\user;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;


/**
 * @Route("/user")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/list",
     * name="user")
     */
    public function index()
    {
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();
        return $this->render('user/index.html.twig',[
            'users' => $users,
        ]);
    }

    /**
     * @Route("/{id}/remove",
     * name="remove",
     * defaults={ "id"=0 })
     */
    public function userRemove($id)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        if (!$user) {
            return $this->redirectToRoute('user');
        } else {
            $entityManager = $this->getDoctrine()->getManager('default');
            $entityManager->remove($user);
            $entityManager->flush();

            return $this->redirectToRoute('user');
        }

    }
}
