<?php
// src/Controller/KeyController.php
namespace App\Controller;


use App\Entity\Key;
use App\Entity\Worker;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/key")
 */
class KeyController extends AbstractController
{
    /**
     * @Route("/list",
     * name="key")
     */
    public function index()
    {
        $repository = $this->getDoctrine()->getRepository(Key::class);
        $notAllocatedKeys = $repository->workerNullOrNot('noWorker');

        $allocatedKeys = $this->getDoctrine()->getRepository(Key::class)->workerNullOrNot('worker');

        return $this->render('key/index.html.twig',[
            'notAllocatedKeys' => $notAllocatedKeys,
            'keys' => $allocatedKeys,
            "searchtext"=>null
        ]);
    }

    /**
     * @Route("/{id}/remove-allocation/{balance}",
     * name="removeallocation")
     */
    public function removeAllocation($id,$balance)
    {
        $key = $this->getDoctrine()->getRepository(Key::class)->find($id);
        $worker = $key->getWorker()->getId();
        if ($key && $worker){
            if ($balance != 0.00){
                return $this->render('key/remove-allocation.html.twig',[
                    'key' => $key,
                ]);
            }else{
                $key->setworker(null);
                $key->setBalance(0.00);
                $key->setAllocatedAt(null);
                $entityManager = $this->getDoctrine()->getManager('default');
                $entityManager->persist($key);
                $entityManager->flush();
                return $this->redirectToRoute('key');
            }
        }
        return $this->redirectToRoute('key');

    }
    /**
     * @Route("/{id}/add-allocation/{worker}",
     * name="addallocation",
     * defaults={ "worker"=0 })
     */
    public function addAllocation($id,$worker)
    {
        $key = $this->getDoctrine()->getRepository(Key::class)->findOneBy(['id'=>$id,'worker'=>null]);
        $workers = $this->getDoctrine()->getRepository(Worker::class)->findBy(['dismiss_at'=>null],["last_name" => "ASC","first_name"=>"ASC"]);
        if ($key){
            if ($worker == 0){
                return $this->render('key/add-allocation.html.twig',[
                    'key' => $key,
                    'workers' => $workers
                ]);
            }else{
                $newHolder = $this->getDoctrine()->getRepository(Worker::class)->find($worker);
                $key->setworker($newHolder);
                $key->setAllocatedAt(date_create(date("Y-m-d")));
                $entityManager = $this->getDoctrine()->getManager('default');
                $entityManager->persist($key);
                $entityManager->flush();
                return $this->redirectToRoute('key');
            }
        }
        return $this->redirectToRoute('key');

    }

    /**
     * @Route("/{id}/broken-key/{broken}",
     * name="brokenkey",
     * defaults={ "broken"=0 })
     */
    public function brokenKey($id,$broken)
    {
        $key = $this->getDoctrine()->getRepository(Key::class)->find($id);
        if ($key){
            if ($broken != 0){
                $key->setStatus(1);
                $entityManager = $this->getDoctrine()->getManager('default');
                $entityManager->persist($key);
                $entityManager->flush();
                return $this->redirectToRoute('key');
            }else{
                return $this->render('key/broken-key.html.twig',[
                    'key' => $key,
                ]);

            }
        }
        return $this->redirectToRoute('key');
    }

    /**
     * @Route("/{id}/add-balance/{add}",
     * name="addbalance",
     * defaults={ "add"=0 })
     */
    public function addBalance(Request $request,$id,$add)
    {
//        dd($request->request->get('balance'));
        $key = $this->getDoctrine()->getRepository(Key::class)->find($id);
        if ($key){
            if($add != 0){
                $key->setBalance($request->request->get('balance'));
                $entityManager = $this->getDoctrine()->getManager('default');
                $entityManager->persist($key);
                $entityManager->flush();
                return $this->redirectToRoute('key');
            }else{
                return $this->render('key/add-balance.html.twig',[
                    'key' => $key,
                ]);
            }

        }
        return $this->redirectToRoute('key');
    }

    /**
     * @Route("/keySearch",
     * name="keySearch")
     */
    public function keySearch(Request $request) {
  		$searchtext=$request->request->get('query');
  		$searchtext=str_replace("  "," ",$searchtext);
  		$searchlike="%".str_replace(" ","%",$searchtext)."%";
  		$entityManager = $this->getDoctrine()->getManager('default');
  		$queryBuilder = $entityManager->getRepository(Key::class)->createQueryBuilder('k');
  		$queryBuilder->where("k.status = 0");
      $queryBuilder->join('k.worker', 'w');
      $queryBuilder->Where("(
        k.reference LIKE :searchtext or
        w.first_name LIKE :searchtext or
        w.last_name LIKE :searchtext
        )");
  		$queryBuilder->orderBy('w.last_name', 'ASC');
      $queryBuilder->orderBy('w.first_name', 'ASC');
  		$queryBuilder->setParameter('searchtext',$searchlike);
  		$keys=$queryBuilder->getQuery()->getResult();
  		if ( !$keys ) {
  			$keys=[];
  		}
      // dd($keys);
  		return $this->render(
  			'key/index.html.twig',[
          'notAllocatedKeys' => null,
          "keys"=>$keys,
          "searchtext"=>$searchtext
        ]
  			);
  	}

}
