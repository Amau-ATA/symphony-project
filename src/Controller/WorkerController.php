<?php
// src/Controller/WorkerController.php
namespace App\Controller;


use App\Entity\Worker;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;


/**
 * @Route("/worker")
 */
class WorkerController extends AbstractController
{
    /**
     * @Route("/list",
     * name="worker")
     */
    public function index()
    {
        $repository = $this->getDoctrine()->getRepository(Worker::class);
        $workers = $repository->findBy(["dismiss_at"=> null], ["last_name"=>"ASC","first_name"=>"ASC"]);

        return $this->render('worker/index.html.twig',[
            'workers' => $workers,
             "searchtext"=>null
        ]);
    }

    /**
     * @Route("/{id}/edit",
     * name="edit",
     * defaults={ "id"=0 })
     */
    public function edit(Request $request,$id): Response
    {
        if ($id != 0) {
            $worker = $this->getDoctrine()->getRepository(Worker::class)->find($id);
            if (!$worker) return $this->redirectToRoute('worker');
        } else {
            $worker = new Worker();
        }

        if ( $request->request->get('id') !== null ){
            $lastName = strtolower($request->request->get('last_name'));
            $firstName = strtolower($request->request->get('first_name'));
            $email = strtolower($request->request->get('email'));
            $department = strtolower($request->request->get('department'));
            $function = strtolower($request->request->get('function'));
            $birthday = date_create($request->request->get('birthday'));
            $takeOnAt = date_create($request->request->get('take_on_at'));

            $worker->setLastName($lastName);
            $worker->setFirstName($firstName);
            $worker->setEmail($email);
            $worker->setPhone($request->request->get('phone'));
            $worker->setDepartment($department);
            $worker->setFunction($function);
            $worker->setTakeOnAT($takeOnAt);
            $worker->setBirthday($birthday);

            $entityManager = $this->getDoctrine()->getManager('default');
            $entityManager->persist($worker);
            $entityManager->flush();

            return $this->redirectToRoute('worker');
        }

        return $this->render('worker/edit.html.twig', [
            'worker' => $worker
        ]);
    }

    /**
     * @Route("/{id}/dismiss",
     * name="dismiss",
     * defaults={ "id"=0 })
     */
    public function dismiss($id)
    {
        $worker = $this->getDoctrine()->getRepository(Worker::class)->find($id);
        if (!$worker) {
            return $this->redirectToRoute('worker');
        } else {
            $worker->setDismissAt(date_create());
            $entityManager = $this->getDoctrine()->getManager('default');
            $entityManager->persist($worker);
            $entityManager->flush();

            return $this->redirectToRoute('worker');
        }

    }

    /**
     * @Route("/workerSearch",
     * name="workerSearch")
     */
    public function workerSearch(Request $request) {
  		$searchtext=$request->request->get('query');
  		$searchtext=str_replace("  "," ",$searchtext);
  		$searchlike="%".str_replace(" ","%",$searchtext)."%";
  		$entityManager = $this->getDoctrine()->getManager('default');
  		$queryBuilder = $entityManager->getRepository(Worker::class)->createQueryBuilder('w');
  		$queryBuilder->where("w.dismiss_at IS NULL");
      $queryBuilder->where("(
  				w.last_name LIKE :searchtext or
  				w.first_name LIKE :searchtext or
  				w.phone LIKE :searchtext or
          w.email LIKE :searchtext or
          w.department LIKE :searchtext or
          w.function LIKE :searchtext
  				)");
  		$queryBuilder->orderBy('w.last_name', 'ASC');
      $queryBuilder->orderBy('w.first_name', 'ASC');
  		$queryBuilder->setParameter('searchtext',$searchlike);
  		$workers=$queryBuilder->getQuery()->getResult();
  		if ( !$workers ) {
  			$workers=[];
  		}
  		return $this->render(
  			'worker/index.html.twig',[
          "workers"=>$workers,
          "searchtext"=>$searchtext 
        ]
  			);
  	}

}
