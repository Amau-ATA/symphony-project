<?php
// src/Controller/DashboardController.php
namespace App\Controller;


use App\Entity\Device;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/dashboard")
 */
class DashboardController extends AbstractController
{

    public function index()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $devices = $this->getDoctrine()->getRepository(Device::class)->findAllOrderedBy('reference');
        $maintenances = $this->getDoctrine()->getRepository(Device::class)->findAllOrderedBy('next_maintenance');
        $restocks = $this->getDoctrine()->getRepository(Device::class)->findAllOrderedBy('next_restock');

        return $this->render('dashboard/index.html.twig',[
            'devices' => $devices,
            'maintenances' => $maintenances,
            'restocks' => $restocks
        ]);
    }

    /**
     * @Route("/{id}/restock/{date}",
     * name="restock",
     * defaults={ "date"=0 })
     */
    public function restock($id,$date)
    {
        $device = $this->getDoctrine()->getRepository(Device::class)->find($id);
        if ($device){
            if ($date != 0){
                $oldDate = date_create($date);
                $newDate = date_add($oldDate, date_interval_create_from_date_string('1 days'));
            }else{
                $nextDate = date_create($device->getNextRestock()->format("Y-m-d"));
                $newDate = date_add($nextDate, date_interval_create_from_date_string('-3 days'));
            }
            return $this->render('dashboard/restock.html.twig',[
                'device' => $device,
                'date' => $newDate
            ]);
        }

        return $this->redirectToRoute('dashboard');

    }

    /**
     * @Route("/{id}/new-date-restock/{date}",
     * name="saverestock")
     */
    public function saveRestock($id,$date)
    {
        $device = $this->getDoctrine()->getRepository(Device::class)->find($id);
        if ($device){
            $device->setNextRestock(date_create($date));
            $entityManager = $this->getDoctrine()->getManager('default');
            $entityManager->persist($device);
            $entityManager->flush();
            return $this->redirectToRoute('dashboard');
        }
        return $this->redirectToRoute('dashboard');
    }


    /**
     * @Route("/{id}/maintenance/{date}",
     * name="maintenance",
     * defaults={ "date"=0 })
     */
    public function maintenance($id,$date)
    {
        $device = $this->getDoctrine()->getRepository(Device::class)->find($id);
        if ($device){
            if ($date != 0){
                $oldDate = date_create($date);
                $newDate = date_add($oldDate, date_interval_create_from_date_string('1 days'));
            }else{
                $today = date_create(date("y-m-d"));
                $newDate = date_add($today, date_interval_create_from_date_string('2 days'));
            }
            return $this->render('dashboard/maintenance.html.twig',[
                'device' => $device,
                'date' => $newDate
            ]);
        }

        return $this->redirectToRoute('dashboard');

    }

    /**
     * @Route("/{id}/new-date-maintenance/{date}",
     * name="savemaintenance")
     */
    public function saveMaintenance($id,$date)
    {
        $device = $this->getDoctrine()->getRepository(Device::class)->find($id);
        if ($device){
            $device->setNextMaintenance(date_create($date));
            $entityManager = $this->getDoctrine()->getManager('default');
            $entityManager->persist($device);
            $entityManager->flush();
            return $this->redirectToRoute('dashboard');
        }
        return $this->redirectToRoute('dashboard');
    }


}
