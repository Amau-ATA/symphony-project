<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Key;
use App\Entity\Worker;
use App\Entity\Device;

class AppFixtures extends Fixture
{
    public function randomHex(){
        return str_pad( dechex( mt_rand( 1000, 9999 ) ), 4, '0', STR_PAD_LEFT);
    }
    public function randomDate($start_date, $end_date){
        $min = strtotime($start_date);
        $max = strtotime($end_date);
        $val = rand($min, $max);
        return date_create(date('Y-m-d H:i:s', $val));
    }
    function dateAdd($date,$daysNumber){
        $formatedDate = $date->format("Y-m-d");
        $newDate = date("Y-m-d", strtotime($formatedDate.'+ '.$daysNumber.' days'));
        return date_create($newDate);
    }
    public function randomFloat($min,$max){
        return mt_rand($min*10,$max*10)/10;
    }

    public function load(ObjectManager $manager)
    {
        // arrays of data used with random function
        $deviceTypes = ["boissons chaudes", "boissons froides", "snack", "repas"];

        $workerFirstNames = ["edouard","amaury","alizée", "rose","tristan","dominique", "jacques","zachary","bertrand","sophie","marie","françois","guillaume","simmon","laura","paul","cedric"];
        $workerLastNames = ["aubry","taureau","lalandre","descharles","jonville","souchay", "guerain", "evain","petit","lecuyer","pageon","gouron","berre","pocola","doli","prane","bon","linux"];
        $workerMailBox = ["gmail.com","outlook.fr","orange.fr","laposte.net","icloud.com"];
        $workerDepartment = ["direction","comptabilité","magasin","atelier"];
        $workerFunction = [
            "direction" => ["directeur","ressources humaine"],
            "comptabilité" => ["comptable"],
            "magasin" => ["magasinier"],
            "atelier" => ["chef d'équipe", "ouvrier polyvalent", "ouvrier de production"]
        ];


        // create data for device x6
        for ($i = 1; $i <= 6 ; $i++){
            //variables
            $reference = "Device".$this->randomHex()."-".$this->randomHex()."-".$this->randomHex();
            $type = $deviceTypes[mt_rand( 0, 3 )];
            $lastRestock = $this->randomDate("2021-06-12","2021-06-22");
            $NextRestock = $this->dateAdd($lastRestock,14);
            $lastMaintenance = $this->randomDate("2021-05-01","2021-06-22");
            $NextMaintenance = $this->dateAdd($lastMaintenance,60);
            // create Device
            $device = new Device();
            $device->setReference($reference);
            $device->setType($type);
            $device->setLastRestock($lastRestock);
            $device->setNextRestock($NextRestock);
            $device->setLastMaintenance($lastMaintenance);
            $device->setNextMaintenance($NextMaintenance);

            $manager->persist($device);
        }
        $manager->flush();

        // create data for Worker x30
        for ($i = 1; $i <= 30 ; $i++){
            //variables
            $firstName = $workerFirstNames[mt_rand( 0, 14 )];
            $lastName = $workerLastNames[mt_rand( 0, 14 )];
            $mail = $firstName[0].".".$lastName."@".$workerMailBox[mt_rand( 0, 4 )];
            $phone = "0".mt_rand( 600000000, 699999999 );
            $department = $workerDepartment[mt_rand( 0, 3 )];
            $arrayLenght = count($workerFunction[$department])-1;
            ($arrayLenght != 0 ) ? $function = $workerFunction[$department][mt_rand( 0, $arrayLenght )] : $function = $workerFunction[$department][0];
            $takeOnAT = $this->randomDate("2010-01-01","2021-04-31");
            $birthday = $this->randomDate("1970-01-01","2001-12-31");

            // create Worker
            $worker = new Worker();
            $worker->setFirstName($firstName);
            $worker->setLastName($lastName);
            $worker->setEmail($mail);
            $worker->setPhone($phone);
            $worker->setDepartment($department);
            $worker->setFunction($function);
            $worker->setTakeOnAt($takeOnAT);
            $worker->setBirthday($birthday);
            $manager->persist($worker);

            $this->setKey($worker, $manager);
        }
        $manager->flush();

        // create data for key x4 where worker_id is null
        for ($i = 1; $i <= 4 ; $i++){
            //variables
            $reference = "key".$this->randomHex()."-".$this->randomHex()."-".$this->randomHex();
            $balance = 0;

            // create Key
            $key = new Key();
            $key->setReference($reference);
            $key->setBalance($balance);
            $key->setstatus(0);
            $manager->persist($key);
        }
        $manager->flush();
    }

    public function setKey(Worker $worker, ObjectManager $manager){
        //variables
        $reference = "key".$this->randomHex()."-".$this->randomHex()."-".$this->randomHex();
        $allocatedAT = $this->randomDate("2020-01-01","2021-06-22");
        $balance = $this->randomFloat(0,10);

        // create Key
        $key = new Key();
        $key->setReference($reference);
        $key->setBalance($balance);
        $key->setstatus(0);
        $key->setAllocatedAt($allocatedAT);
        $key->setWorker($worker);
        $manager->persist($key);
    }
}
