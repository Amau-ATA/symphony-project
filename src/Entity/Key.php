<?php

namespace App\Entity;

use App\Repository\KeyRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=KeyRepository::class)
 * @ORM\Table(name="`key`")
 */
class Key
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $reference;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2,options={"default":0})
     */
    private $balance;

    /**
     * @ORM\Column(type="smallint",options={"default":0})
     */
    private $status;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $allocated_at;

    /**
     * @ORM\ManyToOne(targetEntity="Worker", inversedBy="keys")
     * @ORM\JoinColumn(name="worker_id", referencedColumnName="id")
     */
    private $worker;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getBalance(): ?string
    {
        return $this->balance;
    }

    public function setBalance(string $balance): self
    {
        $this->balance = $balance;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getAllocatedAt(): ?\DateTimeInterface
    {
        return $this->allocated_at;
    }

    public function setAllocatedAt(?\DateTimeInterface $allocated_at): self
    {
        $this->allocated_at = $allocated_at;

        return $this;
    }

    public function getWorker(): ?Worker
    {
        return $this->worker;
    }

    public function setWorker(?Worker $worker): self
    {
        $this->worker = $worker;

        return $this;
    }
}
