<?php

namespace App\Entity;

use App\Repository\DeviceRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DeviceRepository::class)
 */
class Device
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $reference;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $type;

    /**
     * @ORM\Column(type="date")
     */
    private $last_restock;

    /**
     * @ORM\Column(type="date")
     */
    private $next_restock;

    /**
     * @ORM\Column(type="date")
     */
    private $last_maintenance;

    /**
     * @ORM\Column(type="date")
     */
    private $next_maintenance;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getLastRestock(): ?\DateTimeInterface
    {
        return $this->last_restock;
    }

    public function setLastRestock(\DateTimeInterface $last_restock): self
    {
        $this->last_restock = $last_restock;

        return $this;
    }

    public function getNextRestock(): ?\DateTimeInterface
    {
        return $this->next_restock;
    }

    public function setNextRestock(\DateTimeInterface $next_restock): self
    {
        $this->next_restock = $next_restock;

        return $this;
    }

    public function getLastMaintenance(): ?\DateTimeInterface
    {
        return $this->last_maintenance;
    }

    public function setLastMaintenance(\DateTimeInterface $last_maintenance): self
    {
        $this->last_maintenance = $last_maintenance;

        return $this;
    }

    public function getNextMaintenance(): ?\DateTimeInterface
    {
        return $this->next_maintenance;
    }

    public function setNextMaintenance(\DateTimeInterface $next_maintenance): self
    {
        $this->next_maintenance = $next_maintenance;

        return $this;
    }
}
