// ------------------------ KEY
// Broken-key page

$("#broken").change(function () {
    var message = '';
    var selected = $("#broken option:selected").val();
    var button = $("#brokenBut")

    $("#message").empty();
    button.addClass('disabled')
    if(selected != 0){
        message = "<p class='text-center text-brown'> <i class='fas fa-undo-alt mr-3'></i>La clef sera échanger lors de la prochaine visite de notre intervenant. <i class='fas fa-undo-alt ml-3'></i><br> Une nouvelle clef vous sera alors donner avec un transfert des données (porteur et solde)";
        $("#message").append(message);
        button.removeClass('disabled')
    }
})

// add-allocation page
$("#worker").change(function () {
    var selected = $("#worker option:selected").val();
    var button = $("#addAllocationBut");
    var url = '';

    button.addClass('disabled');
    if(selected != 0){
        url = "add-allocation/"+selected;
        button.removeClass('disabled');
        button.attr('href',url)
    }
})

// add-balance page
$("#balance").bind( "keyup change", function () {
    const input = $("#balance");
    var newBalance = parseFloat(input.val());
    var oldBalance =parseFloat(input.attr("min"));
    var button = $("#addBalanceButton");
    console.log(newBalance);
    button.addClass('disabled');
    if(newBalance >= oldBalance){
        button.removeClass('disabled');
    }
})

