-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : mar. 22 juin 2021 à 16:26
-- Version du serveur :  10.4.11-MariaDB
-- Version de PHP : 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `coffe_time_a_taureau`
--

-- --------------------------------------------------------

--
-- Structure de la table `device`
--

CREATE TABLE `device` (
  `id` int(11) NOT NULL,
  `reference` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_restock` date NOT NULL,
  `next_restock` date NOT NULL,
  `last_maintenance` date NOT NULL,
  `next_maintenance` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `device`
--

INSERT INTO `device` (`id`, `reference`, `type`, `last_restock`, `next_restock`, `last_maintenance`, `next_maintenance`) VALUES
(43, 'Device1322-162c-1c70', 'boissons chaudes', '2021-06-19', '2021-07-03', '2021-06-20', '2021-08-19'),
(44, 'Device055a-1e21-21cf', 'snack', '2021-06-14', '2021-06-25', '2021-06-18', '2021-08-17'),
(45, 'Device1c04-05bf-234b', 'boissons froides', '2021-06-14', '2021-06-28', '2021-05-20', '2021-07-19'),
(46, 'Device0daf-1bd0-17aa', 'boissons froides', '2021-06-15', '2021-06-29', '2021-05-08', '2021-07-07'),
(47, 'Device2485-1d3f-10b5', 'repas', '2021-06-21', '2021-07-05', '2021-05-08', '2021-07-07'),
(48, 'Device0bc0-076e-16fc', 'repas', '2021-06-12', '2021-06-26', '2021-05-09', '2021-07-08');

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20210619194946', '2021-06-19 19:49:56', 50),
('DoctrineMigrations\\Version20210619195353', '2021-06-19 19:53:57', 42),
('DoctrineMigrations\\Version20210619200010', '2021-06-19 20:00:16', 127),
('DoctrineMigrations\\Version20210622094334', '2021-06-22 09:43:50', 81);

-- --------------------------------------------------------

--
-- Structure de la table `key`
--

CREATE TABLE `key` (
  `id` int(11) NOT NULL,
  `worker_id` int(11) DEFAULT NULL,
  `reference` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `balance` decimal(5,2) NOT NULL DEFAULT 0.00,
  `status` smallint(6) NOT NULL DEFAULT 0,
  `allocated_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `key`
--

INSERT INTO `key` (`id`, `worker_id`, `reference`, `balance`, `status`, `allocated_at`) VALUES
(275, 362, 'key1bbf-0e42-1dbb', '7.80', 0, '2020-02-27'),
(276, 363, 'key1973-0cc5-116b', '3.90', 0, '2020-06-22'),
(277, 364, 'key1226-1d30-0a08', '6.60', 0, '2020-03-22'),
(278, 365, 'key0b42-2616-1366', '9.60', 0, '2020-07-08'),
(279, 366, 'key1aef-1fda-0405', '6.20', 0, '2020-04-21'),
(280, 367, 'key158e-2518-0ca0', '7.90', 0, '2020-07-15'),
(281, 368, 'key21fa-0fd6-11a0', '1.90', 0, '2020-09-20'),
(282, 369, 'key04d6-0e78-14b7', '5.50', 0, '2020-05-06'),
(283, 370, 'key2310-1c49-1db0', '6.50', 0, '2020-11-11'),
(284, 371, 'key0d42-0658-205c', '9.50', 0, '2020-07-26'),
(285, 372, 'key0cc4-0c28-207b', '1.10', 0, '2020-05-11'),
(286, 373, 'key09c1-1b7a-1d44', '4.50', 0, '2021-06-12'),
(287, 374, 'key20d4-180d-0e1b', '6.40', 0, '2020-06-29'),
(288, 375, 'key242f-102b-1525', '7.60', 0, '2020-06-01'),
(289, 376, 'key207d-168b-131a', '8.40', 0, '2020-09-22'),
(290, 377, 'key1df2-0703-1bab', '5.50', 0, '2020-05-30'),
(291, 378, 'key1fb0-1bb5-0618', '3.80', 0, '2020-09-23'),
(292, 379, 'key13e6-1d79-270c', '3.30', 0, '2020-08-06'),
(293, 380, 'key0885-12bd-1464', '2.30', 0, '2021-04-10'),
(294, 381, 'key1e4a-1a0e-0dda', '1.30', 0, '2020-05-08'),
(295, 382, 'key1b05-11ed-1cc7', '8.90', 0, '2021-02-07'),
(296, 383, 'key0dcf-06a9-0558', '7.80', 0, '2021-02-01'),
(297, 384, 'key176c-1fcf-0e7f', '7.90', 0, '2020-02-09'),
(298, 385, 'key2665-1297-18f5', '1.50', 0, '2020-01-09'),
(299, 386, 'key180b-0748-13da', '6.30', 0, '2020-12-08'),
(300, 387, 'key1704-1249-1fce', '9.30', 0, '2020-12-12'),
(301, 388, 'key1281-0591-124a', '2.30', 0, '2020-10-10'),
(302, 389, 'key1a77-2075-0aa1', '1.00', 0, '2021-05-08'),
(303, 390, 'key1891-0a2d-2678', '2.50', 0, '2020-03-14'),
(304, 391, 'key1482-04dd-05a8', '4.60', 0, '2021-04-18'),
(305, NULL, 'key1d7b-1d82-1492', '0.00', 0, NULL),
(306, NULL, 'key1797-0493-0cee', '0.00', 0, NULL),
(307, NULL, 'key0482-0ea3-143a', '0.00', 0, NULL),
(308, NULL, 'key0d6d-1f45-1ac3', '0.00', 0, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:json)',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `email`, `roles`, `password`, `name`) VALUES
(2, 'admin@localhost.com', '[\"ROLE_ADMIN\"]', '$2y$13$vMyhPxzojGtFDuXKQ1HHjeDSjz9QJvMhC5LJjP7VvN8CJE8WXDXg.', 'admin'),
(3, 'user@localhost.com', '[\"ROLE_USER\"]', '$2y$13$uHJaz8K5czr5SMciM5HngukxXaKR/88BeV8Ozq3gFaNRfL6FCTwrq', 'user');

-- --------------------------------------------------------

--
-- Structure de la table `worker`
--

CREATE TABLE `worker` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `function` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `take_on_at` date NOT NULL,
  `dismiss_at` date DEFAULT NULL,
  `birthday` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `worker`
--

INSERT INTO `worker` (`id`, `first_name`, `last_name`, `email`, `phone`, `department`, `function`, `take_on_at`, `dismiss_at`, `birthday`) VALUES
(362, 'edouard', 'berre', 'e.berre@outlook.fr', '0646688552', 'direction', 'directeur', '2012-10-30', NULL, '1998-12-02'),
(363, 'jacques', 'guerain', 'j.guerain@laposte.net', '0669847987', 'direction', 'ressources humaine', '2010-07-28', NULL, '1988-07-12'),
(364, 'amaury', 'berre', 'a.berre@orange.fr', '0684402315', 'direction', 'ressources humaine', '2019-08-26', NULL, '1983-12-24'),
(365, 'edouard', 'lalandre', 'e.lalandre@outlook.fr', '0664670404', 'magasin', 'magasinier', '2020-09-01', NULL, '2000-01-12'),
(366, 'amaury', 'taureau', 'a.taureau@icloud.com', '0643617229', 'direction', 'directeur', '2014-01-20', NULL, '1998-04-02'),
(367, 'rose', 'taureau', 'r.taureau@laposte.net', '0650568375', 'comptabilité', 'comptable', '2011-02-15', NULL, '1978-01-11'),
(368, 'françois', 'taureau', 'f.taureau@orange.fr', '0659648356', 'comptabilité', 'comptable', '2017-06-26', NULL, '1971-12-30'),
(369, 'sophie', 'taureau', 's.taureau@laposte.net', '0653481525', 'direction', 'ressources humaine', '2010-02-22', NULL, '1975-03-14'),
(370, 'dominique', 'gouron', 'd.gouron@laposte.net', '0679397140', 'atelier', 'ouvrier de production', '2014-03-07', NULL, '2001-08-07'),
(371, 'françois', 'doli', 'f.doli@laposte.net', '0673370465', 'magasin', 'magasinier', '2012-05-08', NULL, '1986-11-06'),
(372, 'laura', 'descharles', 'l.descharles@icloud.com', '0603737716', 'comptabilité', 'comptable', '2016-06-21', NULL, '1977-09-16'),
(373, 'amaury', 'descharles', 'a.descharles@orange.fr', '0674918772', 'direction', 'ressources humaine', '2010-07-06', NULL, '1975-02-25'),
(374, 'amaury', 'guerain', 'a.guerain@orange.fr', '0657832386', 'magasin', 'magasinier', '2018-05-28', NULL, '2000-11-21'),
(375, 'zachary', 'guerain', 'z.guerain@outlook.fr', '0613613873', 'direction', 'ressources humaine', '2015-10-09', NULL, '1984-09-23'),
(376, 'tristan', 'doli', 't.doli@gmail.com', '0648817487', 'comptabilité', 'comptable', '2014-02-15', NULL, '1995-06-11'),
(377, 'edouard', 'descharles', 'e.descharles@gmail.com', '0636734193', 'comptabilité', 'comptable', '2010-05-30', NULL, '1990-10-13'),
(378, 'rose', 'berre', 'r.berre@icloud.com', '0640263062', 'magasin', 'magasinier', '2020-05-25', NULL, '1979-05-10'),
(379, 'alizée', 'evain', 'a.evain@outlook.fr', '0601871011', 'direction', 'directeur', '2017-11-24', NULL, '1987-07-26'),
(380, 'simmon', 'berre', 's.berre@icloud.com', '0693367249', 'atelier', 'chef d\'équipe', '2017-02-03', NULL, '1976-06-09'),
(381, 'bertrand', 'doli', 'b.doli@gmail.com', '0625455956', 'magasin', 'magasinier', '2011-08-30', NULL, '1975-09-07'),
(382, 'françois', 'souchay', 'f.souchay@laposte.net', '0687389656', 'atelier', 'ouvrier polyvalent', '2018-06-11', NULL, '1989-01-01'),
(383, 'françois', 'berre', 'f.berre@outlook.fr', '0652763227', 'direction', 'ressources humaine', '2016-02-20', NULL, '1992-01-20'),
(384, 'amaury', 'guerain', 'a.guerain@outlook.fr', '0685104601', 'direction', 'ressources humaine', '2015-08-12', NULL, '1973-01-20'),
(385, 'tristan', 'aubry', 't.aubry@outlook.fr', '0667233171', 'magasin', 'magasinier', '2011-02-01', NULL, '1970-08-23'),
(386, 'dominique', 'guerain', 'd.guerain@orange.fr', '0670482981', 'magasin', 'magasinier', '2020-12-30', NULL, '1991-02-06'),
(387, 'dominique', 'taureau', 'd.taureau@gmail.com', '0664446558', 'atelier', 'ouvrier de production', '2020-10-25', NULL, '1982-06-04'),
(388, 'simmon', 'taureau', 's.taureau@gmail.com', '0671854226', 'comptabilité', 'comptable', '2017-02-28', NULL, '1995-07-10'),
(389, 'guillaume', 'aubry', 'g.aubry@outlook.fr', '0625739624', 'magasin', 'magasinier', '2015-01-24', NULL, '1983-09-06'),
(390, 'rose', 'berre', 'r.berre@laposte.net', '0635950248', 'direction', 'ressources humaine', '2018-08-23', NULL, '1992-03-25'),
(391, 'laura', 'lecuyer', 'l.lecuyer@outlook.fr', '0656382243', 'comptabilité', 'comptable', '2010-02-25', NULL, '1972-11-15');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `device`
--
ALTER TABLE `device`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `doctrine_migration_versions`
--
ALTER TABLE `doctrine_migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Index pour la table `key`
--
ALTER TABLE `key`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_8A90ABA96B20BA36` (`worker_id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`);

--
-- Index pour la table `worker`
--
ALTER TABLE `worker`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `device`
--
ALTER TABLE `device`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT pour la table `key`
--
ALTER TABLE `key`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=309;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `worker`
--
ALTER TABLE `worker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=392;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `key`
--
ALTER TABLE `key`
  ADD CONSTRAINT `FK_8A90ABA96B20BA36` FOREIGN KEY (`worker_id`) REFERENCES `worker` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
